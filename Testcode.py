import numpy as np
import math
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler
from matplotlib import pyplot as plt

import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation, Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import categorical_crossentropy

phi = np.array([])
theta = np.array([])

#Generate data with angels given phase shift,
#assume that the distance between antenna arrays is
#half of the wave length 
for i in range(181):
    theta = np.append(theta, math.radians(i-90))
    phase = math.degrees(np.pi*np.sin(theta[i]))
    phi = np.append(phi, phase)
    theta[i] = math.degrees(theta[i])
    
    

#collect these two arrays-- angle and phase shift-- within a
#tensor flow XXXX

my_feature= np.array([])
my_label = np.array([])

my_feature = phi
my_feature = my_feature/180

my_label = theta
my_label = my_label/180

#my_test_val = np.array([-30,0,30])


#shuffle the data to remove trends in recording
#my_feature, my_label = shuffle(my_feature, my_label)

##normalize the data
scaler = MinMaxScaler(feature_range=(0,1))
##scaled_train_phase = scaler.fit_transform(train_phase.reshape(-1,1))
##scaled_train_degrees = scaler.fit_transform(train_degrees.reshape(-1,1))
##print(scaled_train_phase)

#Define the function that builds the model
def build_model(my_lr):
    model = Sequential()
    
    model = Sequential([
    Dense(units = 120, input_shape=(1,),activation = 'relu'),
    Dense(units = 50, activation = 'relu'),
    Dense(units = 50, activation = 'relu'),
    Dense(units = 1)
    ])
    
    #Compile model topography
    model.compile(optimizer=Adam(learning_rate =my_lr), loss='mse',metrics=[tf.keras.metrics.RootMeanSquaredError()])

    return model

def train_model(model, feature, label, epochs, batch_size):
    #train the model by using the fit function
    history= model.fit(x=feature, y=label, batch_size=batch_size, epochs=epochs)

    trained_weight = model.get_weights()[0]
    trained_bias = model.get_weights()[1]
    
    # The list of epochs is stored separately from the 
    # rest of history.
    epochs = history.epoch
    
    
    # Gather the history (a snapshot) of each epoch.
    hist = pd.DataFrame(history.history)
    
    # Specifically gather the model's root mean 
    #squared error at each epoch. 
    rmse = hist["root_mean_squared_error"]
    
    return trained_weight, trained_bias, epochs, rmse




#@title Define the plotting functions
def plot_the_model(m, feature, label):
  """Plot the trained model against the training feature and label."""

  # Label the axes.
  plt.xlabel("Phase shift (degrees)")
  plt.ylabel("Signal direction (degrees)")

  # Plot the feature values vs. label values.
  plt.scatter(feature*180, label*180, label="True Data")

  # Create a red line representing the model. The red line starts
  # at coordinates (x0, y0) and ends at coordinates (x1, y1).
  x0 = feature*180
  y0 = m*180
  plt.plot(x0, y0, label="Model", c='y')
  plt.title('ML Signal Finder')
  # Render the scatter plot and the red line.
  plt.legend()
  plt.show()

def plot_the_loss_curve(epochs, rmse):
  """Plot the loss curve, which shows loss vs. epoch."""

  plt.figure()
  plt.xlabel("Epoch")
  plt.ylabel("Root Mean Squared Error")
  
  

  plt.plot(epochs, rmse, label="Loss")
  plt.title('Loss Curve')
  plt.legend()
  plt.ylim([rmse.min()*0.97, rmse.max()])
  plt.xscale("log")
  plt.show()



learning_rate=0.00005
epochs=20000
my_batch_size=181

my_model = build_model(learning_rate)
trained_weight, trained_bias, epochs, rmse = train_model(my_model, my_feature, my_label, epochs, my_batch_size)

y0 = my_model.predict(my_feature)
plot_the_loss_curve(epochs, rmse)
plot_the_model(y0, my_feature, my_label)
